from tabulate import tabulate

import os
from os import path

import json

import cv2 as cv
import imutils
from imutils import face_utils

import ipywidgets as widgets

import matplotlib.pyplot as plt
import numpy as np

import random as rnd

from time import sleep
import math

import subprocess
from scipy.io import wavfile

from google.cloud import storage
from google.cloud import speech_v1
from google.cloud.speech_v1 import enums
from datetime import datetime

from pylab import rcParams

import dlib

import string


def overview(data_folder="transcription folder"):
    folders = list()
    durations = list()
    fps = list()
    speeds = list()
    rected = list()

    for folder in os.listdir(data_folder):
        t = Transcriber(folder, quick=True)

        folders.append(t.get_folder())

        duration = t.get_video_duration()
        durations.append("{min}m {sec}s".format(
            min=int(duration // 60),
            sec=int(duration % 60)
        ))

        fps.append(t.get_fps())
        speeds.append(t.get_speeds_count())

        if t.is_rected():
            rected.append(True)
        else:
            rected.append(None)

    return tabulate({
        "folder, {} total".format(len(folders)): folders,
        "duration": durations,
        "fps": fps,
        "rected": rected,
        "speeds": speeds,
    }, headers="keys")


class Transcriber:
    def __init__(self, folder, speed=100, quick=False, data_folder="transcription folder"):
        self.time_format = "%d-%m-%Y %H:%M:%S"
        self.data_folder = data_folder
        self.bucket_name = "transcriber_for_thesis"

        # Extracted audio filename
        self.audio_name = "audio.wav"

        # Folder with speeds in it name
        self.speed_folder_name = "speeds"

        # Datafile with transcript name
        self.transcript_name = "transcript.json"

        # Name of the extracted lip positions
        self.trace_name = "trace.json"

        # Name of the extracted rectangles positions
        self.rects_name = "rects.json"

        # File with transcript and frame data combined, producing usable training data
        self.usable_words_name = "usable_words.json"

        # Name of the basic info file
        self.basic_info_name = "basic_info.json"

        # Name of the video for this transcript
        self.video_name = ""

        self.folder = None
        self.set_folder(folder)

        self.speed = None
        self.set_speed(speed)

        self.storage_client = None
        self.speech_client = None

        self.output_widget = None

        self.assert_folder_exists()
        self.determine_video_name()

        self.basic_setup()

        # If this is only meant to be used in the overview, dont load the rest of the items
        if quick:
            return

        self.log_in_explicit()
        self.setup_output_widget()

        # Initialize dlib face detector together with the landmark predictor
        self.detector = dlib.get_frontal_face_detector()
        self.predictor = dlib.shape_predictor("shape_predictors/shape_predictor_68_face_landmarks.dat")

    def describe(self):
        print("Folder: {}".format(self.folder))
        print("Speed: {}".format(self.speed))
        print("Video name: {}".format(self.video_name))

        duration = self.get_video_duration()
        print("Duration {min}m {sec}s".format(
            min=duration // 60,
            sec=duration % 60
        ))

        print("Lips are traced: {}".format(self.is_traced()))
        print("Speed is transcribed: {}".format(self.is_transcribed()))
        if self.is_transcribed():
            print("Mean confidence: {}".format(self.mean_confidence()))

    def age(self):
        created = self.get_transcript()["created"]
        created = datetime.strptime(created, self.time_format)

        now = datetime.utcnow().strftime(self.time_format)
        now = datetime.strptime(now, self.time_format)

        age = abs(created-now)

        return age

    def get_folder(self):
        return self.folder

    def set_folder(self, folder):
        self.folder = folder

    def get_speed(self):
        return self.speed

    def set_speed(self, speed):
        self.speed = int(speed)

    def assert_folder_exists(self):
        assert self.folder in os.listdir(self.data_folder), "Error, '{folder}' not found in '{data_folder}'".format(
            folder=self.folder,
            data_folder=self.data_folder
        )

    def determine_video_name(self):
        video_name = ""

        for file in os.listdir(path.join(self.data_folder, self.folder)):
            if file.endswith(".mp4"):
                video_name = file
                break

        assert video_name, "No .mp4 video found in '{}'".format(path.join(self.data_folder, self.folder))

        self.video_name = video_name

    def usable_words_determined(self):
        return self.usable_words_name in os.listdir(self.get_speed_folder())

    def log_in_explicit(self):

        # Explicitly use service account credentials by specifying the private key file.
        self.storage_client = storage.Client.from_service_account_json(
            'Vidtimit Transcription-6b9ad6f8da98.json')

        self.speech_client =\
            speech_v1.SpeechClient.from_service_account_json(
                'Vidtimit Transcription-6b9ad6f8da98.json')

        # Make an authenticated API request
        buckets = list(self.storage_client.list_buckets())
        assert buckets is not None, "Could not log in to Google Services."

    def is_transcribed(self):
        return self.transcript_name in os.listdir(path.join(self.get_speeds_folder(), str(self.get_speed())))

    def is_traced(self):
        return self.trace_name in os.listdir(path.join(self.data_folder, self.folder))

    def is_rected(self):
        return self.rects_name in os.listdir(path.join(self.data_folder, self.folder))

    def mean_confidence(self):
        assert self.is_transcribed(), " get confidence, is not transcribed."

        transcript = self.get_transcript()

        return sum([sentence["confidence"] for sentence in transcript["results"]]) / len(transcript["results"])

    # Sets up basic structure for the folder
    def basic_setup(self):
        # Ensure the speed folder exists
        if not path.exists(self.get_speeds_folder()):
            os.mkdir(self.get_speeds_folder())

        # Ensure this speed version also exists
        if not path.exists(self.get_speed_folder()):
            os.mkdir(self.get_speed_folder())

        # Ensure the audio has been extracted
        if not path.isfile(self.get_base_audio_path()):
            self.extract_audio()

        # Ensure basic info has been extracted to a separate file
        if True or not path.isfile(path.join(self.data_folder, self.folder, self.basic_info_name)):
            self.extract_basic_info()

    def extract_audio(self):
        # Extract the audio from the video in MONO
        command = "ffmpeg -i \"{video_path}\" -ab 160k -ac 1 -ar 44100 -vn \"{audio_path}\"".format(
            video_path=self.get_video_path(),
            audio_path=self.get_base_audio_path()
        )

        subprocess.call(command, shell=True)

    def get_uploaded_audio(self):
        # Edited from https://cloud.google.com/storage/docs/listing-objects

        """Lists all the blobs in the bucket."""
        # Note: Client.list_blobs requires at least package version 1.17.0.

        # Get all the blobs in the bucket
        blobs = self.storage_client.list_blobs(self.bucket_name)

        # Return only the names of the blobs
        return [blob.name for blob in blobs]

    # Returns true if the audio is uploaded to the Google bucket
    def audio_uploaded(self):
        return self.get_bucket_friendly_name() in self.get_uploaded_audio()

    def upload_audio(self):
        # Edited from https://cloud.google.com/storage/docs/uploading-objects#storage-upload-object-python

        """Uploads a file to the bucket."""

        audio_file_name = self.get_audio_path()

        bucket = self.storage_client.bucket(self.bucket_name)
        blob = bucket.blob(self.get_bucket_friendly_name())

        blob.upload_from_filename(audio_file_name)

    def extract_basic_info(self):
        basic_info = dict()

        video_capture = cv.VideoCapture(path.join(self.data_folder, self.folder, self.video_name))

        fps = video_capture.get(cv.CAP_PROP_FPS)
        frames = int(video_capture.get(cv.CAP_PROP_FRAME_COUNT))

        basic_info["fps"] = fps
        basic_info["frames"] = frames

        with open(self.get_basic_info_path(), "w") as json_file:
            json.dump(basic_info, json_file, indent=2)

    def get_video_duration(self):
        return self.get_frame_count()//self.get_fps()

    def get_fps(self):
        with open(self.get_basic_info_path(), "r") as json_file:
            return json.load(json_file)["fps"]

    def get_frame_count(self):
        with open(self.get_basic_info_path(), "r") as json_file:
            return json.load(json_file)["frames"]

    def get_speeds(self):
        return os.listdir(path.join(self.data_folder, self.folder, self.speed_folder_name))

    def get_speeds_count(self):
        return len(self.get_speeds())

    def speed_exists(self):
        return path.exists(self.get_audio_path())

    def create_speed(self):
        base_rate = self.get_base_rate()

        # Divide the speed with 100
        speeded_rate = base_rate / 100

        # multiply it with the new speed
        speeded_rate *= self.speed

        # Cast from float to int
        speeded_rate = int(speeded_rate)

        # Write the new audio file to the speed's folder
        wavfile.write(self.get_audio_path(), speeded_rate, self.get_data())

    # Returns the rate of the audio file
    def get_base_rate(self):
        return wavfile.read(self.get_base_audio_path())[0]

    # Returns the rate of the audio file
    def get_rate(self):
        return wavfile.read(self.get_audio_path())[0]

    # Returns the data of the audio file
    def get_data(self):
        return wavfile.read(self.get_base_audio_path())[1]

    # Sets the output widget as
    def setup_output_widget(self):
        self.output_widget = widgets.Output()

    # Returns and ipywidget output widget that is used for live output in jupyter notebook
    def output(self):
        return self.output_widget

    # Return path to all the speeds
    def get_speeds_folder(self):
        return path.join(self.data_folder, self.folder, self.speed_folder_name)

    # Returns path to the current speed
    def get_speed_folder(self):
        return path.join(self.data_folder, self.folder, self.speed_folder_name, str(self.speed))

    # Returns path to the audio extracted from the video file
    def get_base_audio_path(self):
        return path.join(self.data_folder, self.folder, self.audio_name)

    # Returns path to the audio for the set speed
    def get_audio_path(self):
        return path.join(self.data_folder, self.folder, self.speed_folder_name, str(self.speed), self.audio_name)

    def get_basic_info_path(self):
        return path.join(self.data_folder, self.folder, self.basic_info_name)

    def get_video_path(self):
        return path.join(self.data_folder, self.folder, self.video_name)

    def get_lip_path(self):
        return path.join(self.data_folder, self.folder, self.trace_name)

    def get_rects_path(self):
        return path.join(self.data_folder, self.folder, self.rects_name)

    def get_transcript_path(self):
        return path.join(self.data_folder, self.folder, self.speed_folder_name, str(self.speed), self.transcript_name)

    def get_usable_words_path(self):
        return path.join(self.get_speed_folder(), self.usable_words_name)

    # Performs face detection on every frame of the video, and then traces the lips on frames that contain a face
    def trace(self):

        # Path of the video
        video_name = self.get_video_path()

        # The number of frames in the video
        frame_count = self.get_frame_count()

        # Counter for the valid frames
        valid_frames = 0

        # The latest results
        # latest = list()
        # latest_limit = 50
        # latest_symbols = ["x", "S"]

        # The time that the process started
        time_start = datetime.utcnow()

        # Load the video into a opencv2 video capture object
        video_capture = cv.VideoCapture(os.path.join(video_name))

        # Try to load the first frame
        success, img = video_capture.read()

        # If the video could not be loaded, raise exception
        if not success:
            raise Exception("Could not load the video")

        # List over all the landmarks
        complete_trace = list()

        # Will preserve ratio of points
        respect_ratio = True

        # Keep track the current frame being read
        iteration = 0

        # Keep going until there are no more frames
        while success:

            # Data for this frame will be stored in this dict, start it with the frame number
            mouth_landmarks = dict(
                frame=iteration
            )

            # Resize the image, this speeds up face detection without losing too much accuracy
            # img = imutils.resize(img, width=800)

            # Convert it to grayscale for the face recognition
            gray_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

            # Detect faces in the grayscale image, these are returned as rectangles
            face_rectangles = self.detector(gray_img, 1)

            # If a rect was found, that means a face was found
            if face_rectangles:

                # If a face was detected, mark this frame as a success
                mouth_landmarks["success"] = True

                # Take the first rectangle and use that
                rect = face_rectangles[0]

                # The rectangle returned is a boundary box, convert to a x, y, w, h format
                x = rect.left()
                y = rect.top()

                # Determine the landmark for the region, then translate that to a coordinate in the picture
                landmarks = self.predictor(img, rect)

                # There are 68 landmarks in total detected, but only the last 20 are of the mouth, slice them off here
                # List to hold the points of only the mouth
                landmarks = face_utils.shape_to_np(landmarks)[48:].T

                # The face is detected in the frame, so if the person moves around, this is going to affect the
                # position of the face, so then also the mouth.
                # Append the mouth points but subtract the x and y position of the face to remove movement of the face
                landmarks[0] -= x
                landmarks[1] -= y
                """

                ##############
                w = rect.right() - x
                h = rect.bottom() - y

                plt.imshow(img, cmap="gray")
                plt.show()
                input()

                w = rect.right() - x
                h = rect.bottom() - y
                
                # Slice the image (np array) to fit only the face
                face_img = gray_img[y:y + h, x:x + w]
                
                plt.imshow(face_img, cmap="gray")
                plt.scatter(*landmarks)
                plt.title("Frame {}, dim: {}x{}".format(iteration, *face_img.shape))
                plt.show()
                input()
                """

                # As the person moves closed and further away from the camera, the perceived size of the mouth changes
                # Normalize all the mouth points so that they are all on the same scale
                # Split the coords up for easier code writing
                x_coords, y_coords = landmarks

                # Lower and upper bound
                x_bound = [min(x_coords), max(x_coords)]
                y_bound = [min(y_coords), max(y_coords)]

                # Subtract the lower bound for each value so the minimum is at zero
                x_coords -= x_bound[0]
                y_coords -= y_bound[0]

                # Now to normalize, also subtract the minimum from the maximum to move the max
                max_x = x_bound[1] - x_bound[0]
                max_y = y_bound[1] - y_bound[0]

                # Divide by the local maximum to normalize
                # normalized_x = x_coords / max_x
                normalized_y = y_coords / max_y

                # Divide by the local maximum to normalize
                x_coords = x_coords / max_x
                y_coords = y_coords / max_y

                # After doing the above process, all points will be on a 0 to 1 scale
                # This will make the lips appear to be plotted on a square, which is not the case in reality
                # Shift the points down on the y axis to make it more lifelike.
                if respect_ratio:
                    y_coords *= max_y / max_x
                    normalized_y *= max_y / max_x

                # Store in the dictionary
                mouth_landmarks["x"] = list(x_coords)
                mouth_landmarks["y"] = list(y_coords)

                # Store results in the latest list
                # latest.append(True)

            else:
                # Invalid frame
                mouth_landmarks["success"] = False

                # latest.append(False)

            # Append the dictionary describing the mouth to the list holding the location for every frame
            complete_trace.append(mouth_landmarks)

            # Try to read the next frame, this will fail if the end of the video has been reached
            success, img = video_capture.read()

            # Increase the iteration with one as the current iteration is finished
            iteration += 1

            # Check if the frame was valid
            if mouth_landmarks["success"]:

                # Since this frame was valid, increase the value for valid frames by 1
                valid_frames += 1

            # Print to the output widget
            with self.output():

                # Clear the output before writing the next output
                self.output_widget.clear_output(wait=True)

                # Calculate progress in percentage
                frame_percentage = round((100 / frame_count) * iteration, 2)

                # Calculate how many successful frames in percentage
                valid_percentage = round((100 / iteration) * valid_frames, 2)

                # Print the frame progress as well as the percentage
                print("Frame {}/{} {}%".format(iteration, frame_count, frame_percentage))

                # Print the valid frames as well as the percentage
                print("\nValid frames: {} {}%".format(valid_frames, valid_percentage))

                # Calculate how much time has elapsed
                elapsed_time = datetime.utcnow() - time_start

                # Calculate the expected time to finish the processing
                # Assume that the time remaining is linear with the time spent
                expected_time = 100 / frame_percentage * elapsed_time

                # Calculate remaining time by subtracting the elapsed time
                remaining_time = expected_time - elapsed_time

                # Print the time elapsed
                print("Elapsed time: {}d {}h {}m {}s".format(
                    elapsed_time.days,
                    (elapsed_time.seconds // 3600) % 24,
                    (elapsed_time.seconds // 60) % 60,
                    elapsed_time.seconds % 60)
                )

                # Print the time remaining
                print("Remaining time: {}d {}h {}m {}s".format(
                    remaining_time.days,
                    (remaining_time.seconds // 3600) % 24,
                    (remaining_time.seconds // 60) % 60,
                    remaining_time.seconds % 60)
                )

                """
                while len(latest) > latest_limit:
                    latest = latest[1:]

                for result in latest:
                    print(end=latest_symbols[result])
                """

        # All output gathered will be placed in this file
        output_filename = self.get_lip_path()

        # Dump the dictionary to a json file
        with open(output_filename, "w") as json_file:
            json.dump(complete_trace, json_file, indent=2)

        # Print a confirming message that the process is done
        print("Done")

        # Return all the landmarks. They have been written to file, but return anyway
        return complete_trace

    # Performs face detection on every frame of the video, and stores this rectangle to file
    def detect_face(self):

        # Path of the video
        video_name = self.get_video_path()

        # The number of frames in the video
        frame_count = self.get_frame_count()

        # Counter for the valid frames
        valid_frames = 0

        # The time that the process started
        time_start = datetime.utcnow()

        # Load the video into a opencv2 video capture object
        video_capture = cv.VideoCapture(os.path.join(video_name))

        # Try to load the first frame
        success, img = video_capture.read()

        # If the video could not be loaded, raise exception
        if not success:
            raise Exception("Could not load the video")

        # List over all the rectangles
        all_rects = list()

        # Keep track the current frame being read
        iteration = 0

        # Keep going until there are no more frames
        while success:

            # Data for this frame will be stored in this dict, start it with the frame number
            frame_rect = {"frame": iteration}

            # Resize the image, this speeds up face detection without losing too much accuracy
            # img = imutils.resize(img, width=800)

            # Convert it to grayscale for the face recognition
            gray_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

            # Detect faces in the grayscale image, these are returned as rectangles
            face_rectangles = self.detector(gray_img, 1)

            # If a rect was found, that means a face was found
            if face_rectangles:

                # If a face was detected, mark this frame as a success
                frame_rect["success"] = True

                # Take the first rectangle and use that
                rect = face_rectangles[0]

                # The rectangle returned is a boundary box, convert to a x, y, w, h format
                x = rect.left()
                w = rect.right() - x
                y = rect.top()
                h = rect.bottom() - y

                """
                w = rect.right() - x
                h = rect.bottom() - y
                
                # Slice the image (np array) to fit only the face
                face_img = gray_img[y:y + h, x:x + w]
                
                plt.imshow(face_img, cmap="gray")
                plt.scatter(*landmarks)
                plt.title("Frame {}, dim: {}x{}".format(iteration, *face_img.shape))
                plt.show()
                input()
                """
                # Store in the dictionary
                frame_rect["x"] = x
                frame_rect["w"] = w
                frame_rect["y"] = y
                frame_rect["h"] = h

            else:
                # Invalid frame
                frame_rect["success"] = False

            # Append the dictionary describing the mouth to the list holding the location for every frame
            all_rects.append(frame_rect)

            # Try to read the next frame, this will fail if the end of the video has been reached
            success, img = video_capture.read()

            # Increase the iteration with one as the current iteration is finished
            iteration += 1

            # Check if the frame was valid
            if frame_rect["success"]:

                # Since this frame was valid, increase the value for valid frames by 1
                valid_frames += 1

            # Print to the output widget
            with self.output():

                # Clear the output before writing the next output
                self.output_widget.clear_output(wait=True)

                # Calculate progress in percentage
                frame_percentage = round((100 / frame_count) * iteration, 2)

                # Calculate how many successful frames in percentage
                valid_percentage = round((100 / iteration) * valid_frames, 2)

                # Print the frame progress as well as the percentage
                print("Frame {}/{} {}%".format(iteration, frame_count, frame_percentage))

                # Print the valid frames as well as the percentage
                print("\nValid frames: {} {}%".format(valid_frames, valid_percentage))

                # Calculate how much time has elapsed
                elapsed_time = datetime.utcnow() - time_start

                # Calculate the expected time to finish the processing
                # Assume that the time remaining is linear with the time spent
                expected_time = 100 / frame_percentage * elapsed_time

                # Calculate remaining time by subtracting the elapsed time
                remaining_time = expected_time - elapsed_time

                # Print the time elapsed
                print("Elapsed time: {}d {}h {}m {}s".format(
                    elapsed_time.days,
                    (elapsed_time.seconds // 3600) % 24,
                    (elapsed_time.seconds // 60) % 60,
                    elapsed_time.seconds % 60)
                )

                # Print the time remaining
                print("Remaining time: {}d {}h {}m {}s".format(
                    remaining_time.days,
                    (remaining_time.seconds // 3600) % 24,
                    (remaining_time.seconds // 60) % 60,
                    remaining_time.seconds % 60)
                )

        # All output gathered will be placed in this file
        output_filename = self.get_rects_path()

        # Dump the dictionary to a json file
        with open(output_filename, "w") as json_file:
            json.dump(all_rects, json_file, indent=2)

        # Print a confirming message that the process is done
        print("Done")

        # Return all the landmarks. They have been written to file, but return anyway
        return all_rects

    def sample_rect(self):
        assert self.is_rected(), "Cannot retrieve sample from unrected video."

        # Retrieve the rects file
        with open(self.get_rects_path(), 'r') as file:
            rects = json.load(file)

        rect = {"success": False}
        while not rect["success"]:
            rect = rnd.choice(rects)

        cap = cv.VideoCapture(self.get_video_path())
        cap.set(1, rect["frame"])

        ret, frame = cap.read()

        plt.imshow(cv.cvtColor(frame, cv.COLOR_BGR2RGB))
        plt.gca().get_xaxis().set_visible(False)
        plt.gca().get_yaxis().set_visible(False)

        rect_color = (1, 0, 1)
        alpha = 1

        plt.hlines(rect["y"], rect["x"], rect["x"]+rect["w"], color=rect_color, alpha=alpha)
        plt.hlines(rect["y"] + rect["h"], rect["x"], rect["x"]+rect["w"], color=rect_color, alpha=alpha)
        plt.vlines(rect["x"], rect["y"], rect["y"]+rect["h"], color=rect_color, alpha=alpha)
        plt.vlines(rect["x"] + rect["w"], rect["y"], rect["y"]+rect["h"], color=rect_color, alpha=alpha)

        plt.show()

        return rect

    def get_face_of_frame(self, frame, display=False, color="bgr"):
        assert self.is_rected(), "Cannot get face from video that is not rected."

        accepted_colors = [
            "gray",
            "rgb",
            "bgr"
        ]

        assert color in accepted_colors, "color '{}' not accepted. Accepted colors: {}.".format(
            color, accepted_colors
        )

        # Retrieve the rects file
        with open(self.get_rects_path(), 'r') as file:
            rects = json.load(file)

        # Retrieve the correct rect from all the rects
        rect = rects[frame]

        # If this rect was not successful, return none
        if not rect["success"]:
            return None

        cap = cv.VideoCapture(self.get_video_path())
        cap.set(1, rect["frame"])

        ret, frame = cap.read()
        x = rect["x"]
        w = rect["w"]
        y = rect["y"]
        h = rect["h"]
        face = frame[y:y + h, x:x + w]

        if display:
            plt.imshow(cv.cvtColor(face, cv.COLOR_BGR2RGB))
            plt.gca().get_xaxis().set_visible(False)
            plt.gca().get_yaxis().set_visible(False)

            plt.gca().spines["top"].set_visible(False)
            plt.gca().spines["bottom"].set_visible(False)
            plt.gca().spines["right"].set_visible(False)
            plt.gca().spines["left"].set_visible(False)

            plt.show()

        else:
            if color == "gray":
                return cv.cvtColor(face, cv.COLOR_BGR2GRAY)

            if color == "rgb":
                return cv.cvtColor(face, cv.COLOR_BGR2RGB)

            return face

    def show_face_with_landmarks(self, frame):
        img = self.get_frame(frame)

        # Convert it to grayscale for the face recognition
        gray_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        rgb_img = cv.cvtColor(img, cv.COLOR_BGR2RGB)

        # Detect faces in the grayscale image, these are returned as rectangles
        face_rectangles = self.detector(gray_img, 1)

        # If a rect was found, that means a face was found
        if face_rectangles:

            # Take the first rectangle and use that
            rect = face_rectangles[0]

            # The rectangle returned is a boundary box, convert to a x, y, w, h format
            x = rect.left()
            y = rect.top()

            # Determine the landmark for the region, then translate that to a coordinate in the picture
            landmarks = self.predictor(img, rect)

            landmarks = face_utils.shape_to_np(landmarks)[48:].T
            landmarks[0] -= x
            landmarks[1] -= y

            w = rect.right() - x
            h = rect.bottom() - y

            # Slice the image (np array) to fit only the face
            face_gray_img = gray_img[y:y + h, x:x + w]
            face_rgb_img = rgb_img[y:y + h, x:x + w]

            plt.imshow(face_rgb_img)
            plt.scatter(*landmarks, s=2, c=[[1, 0, 1]], alpha=1)
            plt.title("Frame {}, dim: {}x{}".format(frame, *face_rgb_img.shape))

            plt.gca().get_xaxis().set_visible(False)
            plt.gca().get_yaxis().set_visible(False)

            plt.gca().spines["top"].set_visible(False)
            plt.gca().spines["bottom"].set_visible(False)
            plt.gca().spines["right"].set_visible(False)
            plt.gca().spines["left"].set_visible(False)

            # plt.figure(figsize=(20, 10))

            plt.show()

    def get_frame(self, frame, display=False, color="bgr"):

        accepted_colors = [
            "gray",
            "rgb",
            "bgr"
        ]

        assert color in accepted_colors, "color '{}' not accepted. Accepted colors: {}.".format(
            color, accepted_colors
        )

        cap = cv.VideoCapture(self.get_video_path())
        cap.set(1, frame)

        ret, frame = cap.read()

        if display:
            plt.imshow(cv.cvtColor(frame, cv.COLOR_BGR2RGB))
            plt.gca().get_xaxis().set_visible(False)
            plt.gca().get_yaxis().set_visible(False)

            plt.gca().spines["top"].set_visible(False)
            plt.gca().spines["bottom"].set_visible(False)
            plt.gca().spines["right"].set_visible(False)
            plt.gca().spines["left"].set_visible(False)

            plt.show()

        else:
            if color == "gray":
                return cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

            if color == "rgb":
                return cv.cvtColor(frame, cv.COLOR_BGR2RGB)

            return frame

    def get_mouth_of_frame(self, frame, resize_size=(50, 50), interpolation=cv.INTER_AREA):

        face = self.get_face_of_frame(frame, color="gray")

        resized_face = cv.resize(face, resize_size, interpolation=interpolation)

        # Remove the top half of the image
        bottom_half = resized_face[resize_size[0] // 2:, :]

        # Remove one fifth from each side, removing a total of 40% of pixels
        sides_removed = bottom_half[:, resize_size[1] // 5:-resize_size[1] // 5]

        return sides_removed

    def get_mouth_of_frames(self, frames, resize_size=(50, 50), interpolation=cv.INTER_AREA):
        assert self.is_rected(), "Cannot get face from video that is not rected."

        # Retrieve the rects file
        with open(self.get_rects_path(), 'r') as file:
            rects = json.load(file)

        cap = cv.VideoCapture(self.get_video_path())

        mouths = list()

        for frame in frames:

            # Retrieve the correct rect from all the rects
            rect = rects[frame]

            # If this rect was not successful, return none
            if not rect["success"]:
                return None

            cap.set(1, rect["frame"])

            ret, frame = cap.read()
            x = rect["x"]
            w = rect["w"]
            y = rect["y"]
            h = rect["h"]

            face = frame[y:y + h, x:x + w]

            face = cv.cvtColor(face, cv.COLOR_BGR2GRAY)

            resized_face = cv.resize(face, resize_size, interpolation=interpolation)

            # Remove the top half of the image
            bottom_half = resized_face[resize_size[0] // 2:, :]

            # Remove one fifth from each side
            mouth = bottom_half[:, resize_size[1] // 5:-resize_size[1] // 5]

            # Append the mouth
            mouths.append(mouth)

        return mouths

    def get_bucket_friendly_name(self):
        # Get the video name for this video
        name = self.video_name.replace(".mp4", "")

        # Remove any dashes that might be at the ends, this sometimes happens due to how videos are acquired
        name = name.strip("-")

        # Convert to lowercase for better access
        name = name.lower()

        # Append a speed indicator at the back of the file. The p stands for % (percent)
        name = name+" {}p".format(self.speed)

        # Return the filename
        return name

    def transcribe(self):
        """
        Transcribe long audio file from Cloud Storage using asynchronous speech
        recognition

        Args:
          storage_uri URI for audio file in Cloud Storage, e.g. gs://[BUCKET]/[FILE]
        """

        # The rate is required to send along with the audio file for correct transcribing
        rate = self.get_rate()

        # Configuration of the transcription
        config = {
            "encoding": enums.RecognitionConfig.AudioEncoding.LINEAR16,
            "language_code": "en-US",
            "sample_rate_hertz": rate,
            "enable_word_time_offsets": True
        }

        # The location of the audio file in the google disc
        storage_uri = "gs://transcriber_for_thesis/{}".format(self.get_bucket_friendly_name())

        # Dictionary holding this data
        audio = {"uri": storage_uri}

        #
        operation = self.speech_client.long_running_recognize(config, audio)

        print(u"Waiting for operation to complete...")
        response = operation.result()

        return self.write_to_file(response.results)

    def write_to_file(self, transcription):
        complete_results = dict()

        complete_results["rate"] = self.get_rate()
        complete_results["audio signals"] = len(self.get_data())
        complete_results["created"] = datetime.utcnow().strftime(self.time_format)
        complete_results["volume"] = len(transcription)
        complete_results["folder"] = self.folder
        complete_results["slowed"] = self.speed != 100
        complete_results["speed"] = self.speed
        complete_results["results"] = list()

        for transcribe_result in transcription:
            sentence = transcribe_result.alternatives[0]

            result = dict()
            result["confidence"] = sentence.confidence
            result["transcript"] = sentence.transcript

            result["words"] = list()
            for word in sentence.words:
                word_dict = dict()

                word_dict["word"] = word.word
                word_dict["start"] = float("{}.{}".format(word.start_time.seconds, str(word.start_time.nanos)[0]))
                word_dict["end"] = float("{}.{}".format(word.end_time.seconds, str(word.end_time.nanos)[0]))
                word_dict["duration"] = round(float(word_dict["end"])-float(word_dict["start"]), 1)

                result["words"].append(word_dict)

            complete_results["results"].append(result)

        output_filename = self.get_transcript_path()

        # Dump the dictionary to a json file
        with open(output_filename, "w") as json_file:
            json.dump(complete_results, json_file, indent=2)

        return output_filename

    def get_transcript(self):
        transcript_file = self.get_transcript_path()

        with open(transcript_file, 'r') as file:
            transcript = json.load(file)

        return transcript

    def valid_plot(self, success="darkgreen", failure="red"):
        input_filename = self.get_rects_path()

        with open(input_filename, 'r') as file:
            json_data = json.load(file)

        results = [frame["success"] for frame in json_data]

        current = results[0]

        flips = list()

        for index, result in enumerate(results):
            if current != result:
                flips.append(index)
                current = result

        colors = [failure, success]
        color = results[0]

        start = 0
        for flip in flips:
            plt.hlines(0, start, flip, color=colors[color], linewidth=50)

            color = not color
            start = flip

        plt.show()

    def determine_usable_words(self):

        # Open the file containing the face rectangles
        with open(self.get_rects_path(), 'r') as file:
            rects = json.load(file)

        # Get the transcript
        transcript = self.get_transcript()

        # These will be removed if the word contains them
        illegal_punctuation = [".", ",", "!", "?"]

        # Determine the fps of the video
        fps = self.get_fps()

        # Get the speed of the audio
        speed = self.get_speed()

        # Divide it by hundred so that it can be used as a scalar
        speed /= 100

        # Each word is a key in the dictionary
        # The value is a list containing lip traces for the key
        valid_words = dict()

        # Iterate the sentences in the transcript
        for sentence in transcript["results"]:

            # Iterate the word in each sentence
            for word in sentence["words"]:

                # Skip this word if it is an empty word
                # This occasionally happens due to Google's rounding of timing labels
                if word["duration"] == 0.0:
                    continue

                # Convert to lowercase
                word["word"] = word["word"].lower()

                # Remove illegal punctuations
                for p in illegal_punctuation:
                    word["word"] = word["word"].replace(p, "")

                # Get the start and end of the word
                start = word["start"]
                end = word["end"]

                # Scale the transcript linearly with the speed
                start *= speed
                end *= speed

                # Determine the start and stop of the words
                # Since fps determines how many frames are in a second, multiplying the (scaled) timestamps
                # gives the correct frames
                start_frame = fps*start
                end_frame = fps*end

                # The range for the frames, convenient
                frame_range = (int(start_frame), int(end_frame))

                # If the word contains a single invalid frame, it is deemed invalid
                # Assume it is valid, then disqualify if invalid frame is detected within the time window
                word_is_valid = True

                # Iterate all the frames in the time window and check if they were successful
                for frame in range(*frame_range):

                    # If the value here is False, it is invalid
                    if not rects[frame]["success"]:

                        # One failure is enough to disqualify the word
                        word_is_valid = False
                        break

                    # The face recognition sometimes gives negative values the face location
                    # The reason for this is unknown, perhaps some faces are rotated or upside down?
                    # There are not usable, so if this happens the word is invalid
                    if rects[frame]["x"] < 0 or rects[frame]["y"] < 0 or rects[frame]["w"] < 0 or rects[frame]["h"] < 0:

                        # One failure is enough to disqualify the word
                        word_is_valid = False
                        break

                # If the word was not valid, continue to the next word
                if not word_is_valid:
                    continue

                # The actual word for this word. For machine learning this is the label.
                actual_word = word["word"]

                # Insert this key in the dictionary if it is not already there
                if actual_word not in valid_words:
                    valid_words[actual_word] = list()

                # Append a new list that will hold the lip rect
                valid_words[actual_word].append(list())

                # Go through every frame in the words's windows and add the x and y trace to the
                for frame in range(*frame_range):
                    frame_details = {
                        "frame": frame,
                        "rect": [
                            rects[frame]["x"],
                            rects[frame]["y"],
                            rects[frame]["w"],
                            rects[frame]["h"]
                        ]
                    }

                    # Append to the current word and to the latest list appended
                    valid_words[actual_word][-1].append(frame_details)

        # Name of the output file
        output_filename = self.get_usable_words_path()

        # Dump the valid words to a json file
        with open(output_filename, "w") as json_file:
            json.dump(valid_words, json_file, indent=2)

        # Return the location of the file
        return output_filename

    def get_usable_words(self):
        # Path of the file holding all the usable words collected
        filename = self.get_usable_words_path()

        # Load the file and read it as json
        with open(filename, 'r') as file:
            words = json.load(file)

        return words

    def valid_words_overview(self):
        sorted_summary = sorted([
            (value, key) for key, value in self.valid_words_summary().items()
        ], reverse=True)

        return tabulate({
            "Word, {} total".format(len(sorted_summary)): [pair[1] for pair in sorted_summary],
            "Instances": [pair[0] for pair in sorted_summary]
        }, headers="keys")

    def valid_words_summary(self):

        # Get the json object describing the words
        words = self.get_usable_words()

        # Take the length of each words list to get the count
        summary = {word: len(words[word]) for word in words}

        # Return the summary collected
        return summary

    def valid_words_plot(self):
        print("Work in progress!")
        sorted_summary = sorted([
            (value, key) for key, value in self.valid_words_summary().items()
        ], reverse=False)

        # Split into x and y
        words = [word[1] for word in sorted_summary]
        frequency = [word[0] for word in sorted_summary]

        # Create the figure, make it very wide to fit the labels correctly
        plt.figure(figsize=(10, 30))

        # Plot the bars
        plt.barh(words, frequency)

        # Rotate the column labels to make the easier to read
        plt.xticks(rotation=45, ha="right")

        # Set appropriate title
        plt.title("Valid words retrieved from '{folder}', {speed}% speed".format(
            folder=self.get_folder(),
            speed=self.get_speed())
        )

        # Show the plot
        plt.show()









































