import numpy as np                     # numeric python lib

import matplotlib
import matplotlib.image as mpimg       # reading images to numpy arrays
import matplotlib.pyplot as plt        # to plot any graph
import matplotlib.patches as mpatches  # to draw a circle at the mean contour

from skimage import measure            # to find shape contour
import scipy.ndimage as ndi            # to determine shape centrality

from tabulate import tabulate

import cv2 as cv

import os
import sys
from pprint import pprint


class VidtimitModule:
    def __init__(self):

        # Location of the data
        self.data_folder = 'vidtimit-audiovideo'
        self.haar_face_cascade = cv.CascadeClassifier('face_cascades/haarcascade_frontalface_alt.xml')

        self.all_prompts = self.collect_all_prompts()
        self.all_speakers = self.collect_all_speakers()

        # matplotlib setup
        plt.rcParams['figure.figsize'] = (8, 8)      # setting default size of plots

    # Convert input image from BGR to RGB
    @staticmethod
    def convert_to_rgb(img):
        return cv.cvtColor(img, cv.COLOR_BGR2RGB)

    @staticmethod
    def get_opencv_version():
        return cv.__version__

    # Displays an image without axes
    @staticmethod
    def show_image(image, cmap="gray", title=None):
        plt.imshow(image, cmap=cmap)
        plt.gca().get_xaxis().set_visible(False)
        plt.gca().get_yaxis().set_visible(False)
        if title:
            plt.title(title)
        plt.show()

    def assert_speaker_exist(self, speaker):
        assert speaker in self.all_speakers, "Invalid speaker. Allowed speakers:\n{}".format(
            self.get_tabulated_speakers()
        )
        return True

    def assert_speaker_has_prompt(self, speaker, prompt):
        assert prompt in self.get_prompt_ids_for_speaker(speaker), "Invalid prompt. Allowed prompts:\n{}".format(
            self.get_prompts_for_speaker(speaker)
        )
        return True

    def get_tabulated_speakers(self):
        return tabulate({
            "Male": [speaker for speaker in self.all_speakers if speaker.startswith("m")],
            "Female": [speaker for speaker in self.all_speakers if speaker.startswith("f")]
        }, headers="keys")

    def collect_all_prompts(self) -> dict:
        all_prompts = dict()

        for line in open(os.path.join(self.data_folder, "PROMPTS.txt")).readlines():
            line = line.strip()
            if line.startswith(";"):
                continue

            left_bracket = line.find("(")
            right_bracket = line.find(")")

            prompt_id = line[left_bracket + 1:right_bracket]
            prompt = line[:left_bracket - 1]

            all_prompts[prompt_id] = prompt

        return all_prompts

    def get_all_speakers(self):
        return self.collect_all_speakers()

    def collect_all_speakers(self) -> list:
        return [
            speaker for speaker in os.listdir(self.data_folder)
            if os.path.isdir(os.path.join(self.data_folder, speaker))
        ]

    # Returns a rectangle for the face's location
    def get_face_location(self, gray_image, scale_factor=1.1):
        # let's detect multiscale (some images may be closer to camera than others) images
        face = self.haar_face_cascade.detectMultiScale(gray_image, scaleFactor=scale_factor, minNeighbors=5)
        return face[0]

    def get_prompts_for_speaker(self, speaker):
        self.assert_speaker_exist(speaker)
        ids = self.get_prompt_ids_for_speaker(speaker)
        prompts = [self.all_prompts[prompt_id] for prompt_id in ids]
        frames = list()
        for ID in ids:
            frames.append(len(os.listdir(os.path.join(self.data_folder, speaker, "video", ID))))

        tabulated_prompts = tabulate({
            "ID": ids,
            "Prompt": prompts,
            "Frames": frames,
        }, headers="keys")

        return tabulated_prompts

    def get_prompt_ids_for_speaker(self, speaker):
        return [prompt.strip(".wav") for prompt in os.listdir(os.path.join(self.data_folder, speaker, "audio"))]

    def show_base_image_for_speaker(self, speaker):
        self.assert_speaker_exist(speaker)
        matplotlib.rcParams['figure.figsize'] = [8, 8]
        fig, axis = plt.subplots()

        base_image_loc = os.path.join(self.data_folder, speaker, "video", "head", "001")
        base_image = cv.imread(base_image_loc)

        axis.imshow(self.convert_to_rgb(base_image))
        axis.get_xaxis().set_visible(False)
        axis.get_yaxis().set_visible(False)

        plt.title("Speaker {}".format(speaker))
        plt.show()

    def get_frames_of_prompt(self, speaker, prompt):
        self.assert_speaker_exist(speaker)
        self.assert_speaker_has_prompt(speaker, prompt)

        return [frame for frame in os.listdir(os.path.join(self.data_folder, speaker, "video", prompt))]

    def get_prompt(self, prompt_id):
        if prompt_id in self.all_prompts:
            return self.all_prompts[prompt_id]
        return "ERROR. Prompt '{}' not found.".format(prompt_id)

    def get_frames_folder(self, speaker, prompt):
        return os.path.join(self.data_folder, speaker, "video", prompt)

    def get_cropped_face(self, image):
        x, y, w, h = self.get_face_location(cv.cvtColor(image, cv.COLOR_BGR2GRAY))

        # Crop the image to only the face region using numpy slicing
        cropped_face = image[y:y + h, x:x + w]

        return cropped_face































